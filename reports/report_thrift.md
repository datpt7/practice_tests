# THRIFT
## 1. Khái niệm về Microservices
* Microservices là một kiểu kiến trúc phần mềm. Các module trong phần mềm sẽ được chia thành các service, sao cho mỗi service đảm nhận một chức năng. 
* Mỗi service sẽ dược đặt trên một server riêng và có một database riêng.
* Các service giao tiếp với nhau thông qua mạng (HTTP hoặc MessageQueue).
* Microservices ra đời nhằm cải thiện những nhược điểm của Monolith Application khi triển khai trên hệ thống lớn.
* Ưu điểm của microservices:
    * Chính vì chia làm nhiều service như vậy, Microservices giúp dễ nâng cấp và mở rộng hệ thống.
    * Khả năng sập cả hệ thống khi một module bị lỗi rất thấp.
    * Ở mỗi service ta có thể viết các ngôn ngữ các nhau, điều này rất hữu ích vì mỗi ngôn ngữ có một thế mạnh riêng.
* Nhược điểm của microservices:
    * Bởi giao tiếp qua mạng nên tốc độ có tụt lại so với Monolith.
    * Cần phải có phương án cho việc đồng nhất dữ liệu.
    * Việc quản lý nhiều service có phần rắc rối.
    * Cần một đội ngũ đủ mạnh để thiết kế và triển khai hệ thống.
## 2. Khái niệm về Thrift
* Apache thrift là một RPC thuộc loại stack software độc lập với ngôn ngữ lập trình với một cơ chế sinh code tự động có liên kết cho RPC.
* Thrift cho phép các xây dựng RPC Client và Server bằng cách chỉ việc định nghĩa kiểu dữ liệu và service interface trong một file định nghĩa đơn giản.
* Thrift IDL (Interface Definition Language) là một ngôn ngữ giao tiếp giúp sinh code của client và server như đã định nghĩa giúp cung cấp các server đa nền tảng.
* Thrift hỗ trợ hơn 20 ngôn ngữ lập trình.

## 3. Khái niệm Physical, Transport, Protocol, Processor và Application trong Thrift
### 3.1. Physical.
* Physical là tầng dưới cùng của Thrift, tầng Protocal sẽ ghi vào đây.
* Dễ hiểu hơn, physical là các thiết bị vật lý tham gia vào hệ thống.
### 3.2. Transport
* Là tầng nằm trên Physical, chịu trách nhiệm về việc dữ liệu được truyền đi như thế nào, tầng này sẽ đọc và ghi vào Physical.
* Một số kiểu transport: 
    * TSocket: Sử dụng blocking socket I/O để vận chuyển.
    * TFramedTransport: Dữ liệu được truyền theo frame, ở kiểu này thì server phải thuộc loại non-blocking.
    * TFileTransport: Dữ liệu được ghi ra file (Chưa được impliment với java).
    * TMemoryTransport: Sử dụng memory để đọc ghi.
    * TZlibTransport: Dữ liệu được nén bởi zlib, cần phải kết hợp với các kiểu transport khác (Chưa được impliment với Java).
### 3.3. Protocol
* Là tầng nằm trên Transport, chịu trách nhiệm về việc dữ liệu gì được truyền đi bằng cách cung cấp các serialize và deserialize.
* Thrift hỗ trợ cả 2 giao thức đó là text và binary.
* Một số kiểu của Protocol:
    * TBinaryProtocol: Mã hóa các giá trị số dưới dạng binary.
    * TCompactProtocol: Rất nhanh, mã hóa các dữ liệu cồng kềnh.
    * TDenseProtocol: Giống với TCompactProtocol nhưng dữ liệu truyền đi không có meta data, nhưng meta data sẽ được thêm lại khi dữ liệu được trả về (Chưa được impliment với Java).
    * TJSONProtocol: Sử dụng kiểu JSON khi mã hóa dữ liệu.
    * TSimpleJSONProtocol: Cũng sử dụng kiểu json nhưng thuộc loại write-only.
    * TDebugProtocol: Sử dụng kiểu text để hỗ trợ debug.
### 3.4. Processor
* Processor nhận vào các tham số như một đầu vào và một đầu ra.
* Đọc dữ liệu từ đầu vào, sau đó xử lý dữ liệu và ghi dữ liệu vào đầu ra.
* Server sẽ truyền dữ liệu đầu vào đến Processor, các loại server cơ bản:
    * TSimpleServer: Không hỗ trợ đa luồng, sử dụng blocking I/O. Sử dụng để test là chủ yếu
    * TThreadPoolServer: Hỗ trợ đa luồng nhưng vẫn sử dụng blocking I/O.
    * TNonblockingServer: Hỗ trợ đa luồng, sử dụng non-blocking I/O.
### 3.5. Các kiểu dữ liêu của Thrift
#### 3.5.1. Các kiểu dữ liệu cơ bản
* bool: giá trị logic (true hoặc false)
* byte: giá trị nguyên 8bit có dấu.
* i16, i32, i64: tương ứng giá trị nguyên 16bit, 32bit và 64bit có dấu.
* double: giá trị số thực 64bit.
* string: giá trị văn bảng sử dụng mã hóa UTF-8.
#### 3.5.2. Các kiểu dữ liệu đặc biệt
* binary: giá trị một chuỗi các byte không được mã hóa.
#### 3.5.3. Structs
* Struct là một tập các trường có kiểu dữ liệu cơ bản, tương tự như struct trong C.
#### 3.5.4. Containers
* list: là interface tương ứng với C++ STL vector, Java ArrayList,...
* set: là interface tương ứng với STL set, Java HashSet,...
* map: là interface tương ứng với STL map, Java HashMap,...
#### 3.5.5. Exception
* Kế thừa các base class Exception tương ứng với từng ngôn ngữ lập trình.
#### 3.5.6. Service
* Bao gồm một tập các hàm, mỗi hàm có một tập các tham số và một kiểu trả về.

### 3.6. Client Application và Server Application
* Client Application là một ứng dụng sẽ gửi yêu cầu đến một Server Application sau đó nhận giá trị trả về từ Server.
* Server Application là một ứng dụng sẽ nhận yêu cầu thực thi từ các Client Application, nhận input, thực thi tác vụ sau đó trả output về cho Client Application.
## 4. Phân biệt và so sánh Thrift với RESTFull API
### 4.1. Giống nhau
* Cả Thrift và RESTFull API đều hỗ trợ cho việc giao tiếp giữa các service trong microservices.
### 4.2. Khác nhau
|       | Thrift     | RESTFull API |
| :---          | :---        | :--         |
|   Phổ biến    | Sử dụng nhiều trong các dự án liên quan đến BigData | Sử dụng nhiều trong các ứng dụng Web |
|   Hiệu suất   | Hiệu suất cao | Hiệu suất thấp hơn so với Thrift|
|   Kiểu định dạng   | Hỗ trợ cả text (Json) và binary | Chỉ hỗ trợ text (Json, XML, ...) |
|   Giao thức | Hỗ trợ cả HTTP và TCP/IP | Chỉ hỗ trợ HTTP |
|   Lợi thế    | Đáp ứng được nhiều use case  | Định dạng dữ liệu linh hoạt|




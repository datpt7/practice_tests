#DATABASE


## 1. Liệt kê và phân loại các database đang được sử dụng rộng rãi nhất

| Database      | Phân loại     |
| :---          | :----:        | 
| Oracle        | RDBMS         |
| MySQL         | RDBMS         |
| MS SQL Server | RDBMS         |
| PostgreSQL    | RDBMS         |
| MongoDB       | Document database|
| IBM DB2       | RDBMS         |
| Redis         | Key-Value database|
| Elasticsearch | Document database|
| Cassandra     | Columnar database|
| MariaDB       | Columnar database|

## 2. Khái niệm về các loại database, so sánh ưu nhược điểm từng loại

| Loại database     | Khái niệm     | Ưu điểm       | Nhược điểm        |
| :--- | :--- | :--- | :--- |
| RDBMS | Tổ chức dữ liệu theo các bảng và có quan hệ với nhau để giảm thiểu sự dư thừa dữ liệu đồng thời vẫn đảm bảo sự hiệu quả trong lưu trữ và truy xuất dữ liệu | + Phù hợp với hầu hết các loại dữ liệu thường dùng nhờ vào cấu trúc đơn giản <br> + Cập nhật dữ liệu nhanh: khi một record được cập nhật, tất cả các record liên quan đến nó cũng sẽ được cập nhật theo <br> + Hỗ trợ atomic transaction giúp tránh sai xót của dữ liệu <br>| + Dữ liệu ở các bảng càng lớn thì thời gian truy xuất càng lâu<br> + Mở rộng database bằng cách thêm bộ nhớ cho máy tính (Vertical Scaling), mà bộ nhớ thì có giới hạn <br>|
| Document database | Tổ chức dữ liệu một cách tự do không theo một lược đồ cố định nào cả | + Cho phép lưu trữ với nhiều cấu trúc khác nhau <br> + Thời gian truy xuất nhanh, không phụ thuộc vào kích thức của database, hỗ trợ các tác vụ song song <br> + Mở rộng database bằng cách thêm máy tính vào hệ lưu trữ phân tán (Horizontal Scaling), không giới hạn bộ nhớ <br> | + Cập nhật dữ liệu chậm vì dữ liệu được nhân bản và lưu ở nhiều máy khác nhau <br> + Không hỗ trợ atomic transaction |
| Columnar database | Tổ chức dữ liệu theo dạng cột thay vì dạng hàng như RDBMS | + Truy xuất nhanh hơn dạng hàng vì chỉ tập trung truy xuất những giá trị cần thiết <br> + Lưu ở dạng cột giúp cho việc phân tích dễ dàng hơn <br> | + Đối với một số truy vấn cần nhiều cột thì lưu trữ theo dạng hàng là sự lựa chọn hợp lý hơn <br> + Thêm dữ liệu mới tốt thời gian hơn dạng hàng thì cần phải thêm từng cột riêng lẻ <br> |
| Key-value database | Tổ chức dữ liệu theo dạng key-value, có nghĩa là với mỗi key sẽ được gắn với một giá trị | + Truy vấn rất nhanh bởi các key là unique và phần lớn key-value đưuọc lưu trữ trên RAM <br> + Cả key và value có thể là bất cứ loại dữ nào từ đơn giản đến phức tạp <br> | Tốn chi phí hơn bởi hoạt động trên RAM | 
| Graph database | Tổ chức dữ liệu để coi các mối quan hệ giữa các dữ liệu là quan trọng như nhau đối với bản thân dữ liệu | + Tốc độ truy vấn chỉ phụ thuộc vào số lượng mối quan hệ cụ thể chứ không phụ thuộc vào lượng dữ liêu <br> + Kết quả được trả về real time <br> + Những mối quan hệ được thể hiện rõ ràng và dễ quản lý <br> + Cấu trúc linh hoạt và nhanh nhẹn <br> | + Khó mở rộng quy mô <br> + Không có ngôn ngữ truy vấn thống nhất <br> |


## 3. Khái niệm về Docker/Container 
### 3.1. Khái niệm về Docker
* Docker là nền tảng cho phép dựng, kiểm thử và triển khai ứng dụng một cách nhanh chóng.
* Docker đóng gói phần mềm vào các đơn vị tiêu chuẩn hóa được gọi là container.
* Docker giúp triển khai và thay đổi quy mô ứng dụng vào bất kỳ môi trường nào và biết chắc rằng ứng dụng sẽ chạy được.

### 3.2. Khái niệm về Container
* Container là đơn vị phần mềm cung cấp cơ chế đóng gói ứng dụng, mã nguồn, thiết lập, thư viện… vào một đối tượng duy nhất. 
* Ứng dụng sau khi được đóng gói có thể hoạt động một cách nhanh chóng và hiệu quả trên các môi trường khác nhau.

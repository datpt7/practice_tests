
# *REPORT GIT*


## *1. Khái niệm Working space, Staging và History*

### *1.1. Working space*
* ***Working space*** là những thư mục (bao gồm cả các file trong thư mục) được đặt trong sự quản lý của Git mà mọi người đang thực hiện công việc trong thực tế.
* Nói một cách dễ hiểu hơn, ***Working space*** là thư mục ***Repository*** đang làm việc trong máy tính.

### *1.2. Staging*
* ***Staging*** là khu vực sẽ lưu trữ những thay đổi trên các file được add từ ***Working space*** để nó có thể được commit (vì muốn commit tập tin nào thì tập tin đó phải nằm trong ***Staging***).
* Nói một cách dễ hiểu hơn, ***Staging*** là nơi lưu những file được add từ ***Working space*** và chuẩn bị được commit lên ***Local resposity***.

### *1.3. History*
* ***History*** là lịch sử tất cả các commit trong ***Branch*** hiện tại, trong đó bao gồm những thông tin: *mã hash của commit, mô tả, người tạo commit và ngày tạo commit*.
* History là log, để xem ***History*** ta sử dụng lệnh `git log`. Ngoài ra ta có thể thêm một vài đối số để hiển thị History theo đúng nhu cầu của mình: `--oneline`, `--graph`, [xem thêm](https://git-scm.com/docs/git-log)
---


## *2. Khái niệm Local repository và Remote repository*

### *2.1. Local repository*
* ***Local Repository*** là repository nằm trên chính máy tính của chúng ta, repository này sẽ đồng bộ hóa với ***Remote repository*** bằng các lệnh của git.

### *2.2 Remote repository*
* ***Remote Repository*** là repository được cài đặt trên server chuyên dụng. Ví dụ: *GitHub, GitLab, Bitbucket,...*
---


## *3. Khái niệm và lệnh git branch, git merge, git rebase*

### *3.1. Branch và lệnh git branch*

#### *3.1.1. Branch*
* ***Branch*** là những phân nhánh ghi lại luồng thay đổi của lịch sử, các hoạt động trên mỗi branch sẽ không ảnh hưởng lên các branch khác nên có thể tiến hành nhiều thay đổi đồng thời trên một ***Repository***, giúp giải quyết được nhiều nhiệm vụ cùng lúc.
* Nói một cách dễ hiểu hơn: git quản lý các phiên bản source dưới dạng cây có nhiều ***Branch***, mỗi ***Branch*** là một phiên bản.

#### *3.1.2. Các lệnh cơ bản liên quan đến Branch*
* Tạo một branch: `git branch <branch_name>`.
* Tạo một branch và di chuyển đến branch đó: `git checkout -b <branch_name>`.
* Xóa một branch ở phía local: `git branch -d <branch_name>`. 
> **_Lưu ý:_** Cách này chỉ xóa một brach khi đang ở branch khác và branch được xóa phải được merge vào branch hiện tại và đã được push lên remote nếu nó có liên kết với branch remote. Nếu bạn muốn xóa branch một cách bất chấp chỉ cần thay `-d` thành `-D`.
* Xóa một branch ở remote được lưu ở phía local: `git branch --delete --remotes <remote_name>/<branch_name>`. Một cách khác: đổi `--delete --remotes` thành `-d -r`.
* Xóa một branch ở remote: `git push origin --delete <branch_name>`. 
> **_Lưu ý:_** Branch đang đứng ở local phải cùng tên với branch được xóa ở remote.
* Chuyển sang một branch khác: `git checkout <another_branch_name>`.
* Đổi tên branch hiện tại: `git branch -m <new_branch_name>`.

### *3.2. Merge và lệnh git merge*

#### *3.2.1. Merge*
* ***Merge*** là việc hợp nhất các Branch độc lập thành một nhánh duy nhất.
* Sử dụng ***Merge*** nếu muốn sắp xếp các commit theo mặc định.
* Log khi dùng ***Merge***hơi khó xem.

#### *3.2.2. Git merge*
* Gộp ***Branch*** khác vào branch hiện tại: `git merge <another_branch_name>`.


### *3.3. Rebase và git rebase*

#### *3.3.1. Rebase*
* ***Rebase*** cũng có chức năng tương tự như Merge là gộp các ***Branch*** lại với nhau, tuy nhiên có sự khác biệt về cách thực hiện và nội dung trong log flie so với Merge.
* Sử dụng ***Rebase*** nếu muốn commit thuộc về ***Branch*** hiện tại luôn luôn là mới nhất.
* Log khi dùng ***Rebase*** rất dễ nhìn.

#### *3.3.2. Git rebase*
* Gộp ***Branch*** khác vào branch hiện tại: `git rebase <another_branch_name>`.
---



## *4. Framework GitFlow: ý nghĩa các nhánh master, develop, features/\*, releases/\*, hotfixes/\*.*


### *4.1. Framework Git Flow*
* ***Git Low*** là một ***Branching model tool*** cung cấp bộ các ***Barnch*** đi kèm với các quy ước và ý nghĩa riêng tương ứng với mỗi ***Branch***.
* Bộ các ***Branch*** được dùng phổ biến nhất là: ***Master***, ***Develope***, ***Features***, ***Release*** và ***Hotfixes***.

### *4.2. Master Branch*
* ***Master*** được coi là ***Branch*** chính với HEAD phản ánh trạng thái prodution-ready.
* ***Master*** là ***Branch*** mặc định khi tạo mới một ***Repository***.

### *4.3. Develop Branch*
* ***Develop*** được coi là Branch chính với HEAD phản ảnh trạng thái thay đổi mới nhất trong quá trình phát triển, chuẩn bị cho Release tiếp theo.
* Các Features và Feature Branch sẽ được tạo từ Branch này.
* Đây là Branch được dùng nhiều nhất trong quá trình phát triển dự án.

### *4.4. Release Branch*
* ***Release*** là Branch được dùng để làm các công việc để chuẩn bị đưa lên Master Branch, ngoài ra còn để fix một vài bug nhỏ còn tồn đọng và chuẩn bị meta-data (version number, build dates...).
* Quy ước: 
  * Tách từ: Develop
  * Merge vào: Develop và Master.
  * Đặt tên: release/\*.
### *4.5. Hotfix Branch*
* ***Hotfix*** là Branch dùng để fix các bug nghiêm trọng xảy ra sau khi sản phẩm đã được Release.
* Công việc ở Hotfix cũng giống như ở Release chỉ khác ở chỗ là không được lên kế hoạch tủ trước.
* Quy ước:
  * Tách từ: Master
  * Merge vào: Develop và Master
  * Đặt tên: hotfixes/\*
 
### *4.6. Feature Branch*
* ***Feature*** là Branch dùng để thực hiện các chức năng mới cho sản phẩm.
* Quy ước:
  * Tách từ: Develop
  * Merge vào: Develop
  * Đặt tên: Tùy ý, miễn clean là được, trừ các tên của các Branch ở trên.
 

# LUIGI

# 1. Khái niệm Data Pipeline
* Data Pipeline là một bộ các quá trình chuyển data từ một hoặc nhiều nguồn sang một nguồn đích nhằm mục đích lưu trữ hoặc phân tích.
* Quá trình đó bao gồm cả việc chuyển đổi data (trasformation, filtering, grouping,... hoặc có thể áp dụng một thuật toán cụ thể) sao cho phù hợp với mục đích.
* Ouput của quá trình này là Input của quá trình kia.
* ETL là một Data Pipeline cụ thể bao gồm 3 quá trình:
    * Extract: Lấy data từ data source.
    * Transform: Thay đổi data sao cho phù hợp với mục đích.
    * Load: Chuyển các data đã được sửa đổi vào data destination.
* Có 3 kiểu kiến trúc cho data pipeline:
    * Batch-based data pipeline: Lập lịch để thực hiện các data pipeline.
    * Streaming data pipeline: Data pipeline được thực hiện ngay khi có thay đổi từ datasource.
    * Lambda Architecture: Kết hợp cả batch và streaming data pipeline.
# 2. So sánh các Framework Data Pipeline phổ biến hiện nay: Luigi và Airflow

## 2.1. Giống nhau
* Sử dụng Python.
* Sử dụng một nút duy nhất để biểu diễn đường đi dữ liệu.
* Sử dụng các tiêu chuẩn cấu trúc dữ liệu.
* Cho phép user định nghĩa các Task, Command và Conditinal paths.
* Cho phép user trực quan hóa Data pipeline.
* Cả 2 đều open-source.
## 2.2. Khác nhau

||Airflow|Luigi|
|:--:|:--|:--|
|Khả năng sử dụng| + Dễ sử dụng hơn cho người mới bắt đầu <br> + Cho phép user có thể xem các DAG task trước khi pipeline được thực thi| + Khó sử dụng cho người mới bắt đầu <br> + User không biết code của mình thật sự đang chạy như thế nào trong các task|
|Khả năng mở rộng| Nhờ vào tính năng LocalScheduler, user có thể tách các task ra khỏi những crons, do đó việc mở rộng dễ dàng hơn| Những task được liên kết chặt chẽ với các cron, do đó việc mở rộng và quản lý sẽ gặp khó khăn|
|Phổ biến| Có cộng đồng người dùng tăng vọt trong các năm gần đây | Cộng đồng người dùng vẫn tăng nhưng hiện tại không nhiều bằng Airflow |

# 3. Các khái niệm ExternalTask, WrapperTask, Dependency Task, Task Scheduler trong Luigi
## 3.1. Khái niệm ExternalTask
* Là một subclass tham chiếu đến các external dependency.
* Là task không có method run.
* Thường được sử dụng để kiểm tra các file hoặc folder có tồn tại hay không.
## 3.2. Khái niệm WrapperTask
* Là một task bao gồm nhiều task khác.
* Task này cũng không có method run.
* Phương thức complete(): Nếu task có bất kì output nào, trả về `true` nếu tất cả ouput tồn tại, ngược lại trả về `false`.
## 3.3. Khái niệm Dependency Task
* Là mối quan hệ giữa các task trong Luigi.
* Phương thức requires() giúp trả về những task nào cần được thực hiện trước task hiện tại.
* Giao diện web sẽ hiển thị các dependency graph giúp user kiểm tra lại xem trình tự các task có đúng như code không.
* Thường là task đầu của một pipeline, trigger các task khác.
## 3.4. Task Scheduler
* Centralized scheduler servers có 2 mục đích:
    * Giúp đảm bảo rằng không có 2 instance của một jọb đang được chạy.
    * Cung cấp một biểu đồ cho những gì đang được thực hiện.
* Central scheduler không thực thi bất kì thứ gì cho user hoặc giúp user thực hiện các job song song.
